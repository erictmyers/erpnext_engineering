## Engineering

App for adding item revisions and other engineering-related customizations to ERPNext.

#### License

GNU General Public License v2

https://gitlab.com/erictmyers/erpnext_engineering.git
